---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2IndexerOverviewIndexSet

## SYNOPSIS
Get overview of current indexing state for the given index set, including deflector config, cluster state, index ranges & message counts

## SYNTAX

```
Get-Graylog2IndexerOverviewIndexSet [-id] <String> [<CommonParameters>]
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2IndexerOverviewIndexSet -id "5b6061ad4a34162461964bc2"
```

## PARAMETERS

### -id
Id of an IndexSet

TODO: check if indexSetId exists

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable.
For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
