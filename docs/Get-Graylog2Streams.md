---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2Streams

## SYNOPSIS
Get a list of all streams

## SYNTAX

```
Get-Graylog2Streams
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2Streams
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
