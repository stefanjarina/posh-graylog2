---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2ClosedIndices

## SYNOPSIS
Get a list of closed indices that can be reopened.

## SYNTAX

```
Get-Graylog2ClosedIndices
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2ClosedIndices
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
