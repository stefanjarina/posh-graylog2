---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2Node

## SYNOPSIS
Information about this node

## SYNTAX

```
Get-Graylog2Node
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2Node
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
