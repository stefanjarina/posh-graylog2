---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2Plugins

## SYNOPSIS
List all installed plugins on this node.

## SYNTAX

```
Get-Graylog2Plugins
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2Permissions
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
