---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2Dashboards

## SYNOPSIS
Get a list of all dashboards and all configurations of their widgets

## SYNTAX

```
Get-Graylog2Dashboards
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2Dashboards
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
