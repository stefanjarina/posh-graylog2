---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2ThreadDumpText

## SYNOPSIS
Get a thread dump in plain text format

## SYNTAX

```
Get-Graylog2ThreadDumpText
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2ThreadDump
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
