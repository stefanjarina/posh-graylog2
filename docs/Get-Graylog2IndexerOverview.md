---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2IndexerOverview

## SYNOPSIS
Get overview of current indexing state, including deflector config, cluster state, index ranges & message counts

## SYNTAX

```
Get-Graylog2IndexerOverview
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2IndexerOverview
```

TODO: return custom object

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
