---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2BufferInformation

## SYNOPSIS
Get current utilization of buffers and caches of this node

## SYNTAX

```
Get-Graylog2BufferInformation
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2BufferInformation
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
