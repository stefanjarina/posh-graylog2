---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# New-Graylog2CycleDeflector

## SYNOPSIS
Cycle deflector to new/next index

## SYNTAX

```
New-Graylog2CycleDeflector
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
New-Graylog2DeflectorConfiguration
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
