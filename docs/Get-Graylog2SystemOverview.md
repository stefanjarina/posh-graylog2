---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2SystemOverview

## SYNOPSIS
Display System information

## SYNTAX

```
Get-Graylog2SystemOverview
```

## DESCRIPTION
Display Graylog2 Instance System information

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2System
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
