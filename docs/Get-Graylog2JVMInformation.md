---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2JVMInformation

## SYNOPSIS
Get JVM information

## SYNTAX

```
Get-Graylog2JVMInformation
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2JVMInformation
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
