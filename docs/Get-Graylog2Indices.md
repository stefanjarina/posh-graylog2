---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2Indices

## SYNOPSIS
Get a list of all open, closed and reopened indices

## SYNTAX

```
Get-Graylog2Indices
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2Indices
```

TODO: Return better object

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
