---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2DeflectorConfiguration

## SYNOPSIS
Get deflector configuration.
Only available on master nodes.

## SYNTAX

```
Get-Graylog2DeflectorConfiguration
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2DeflectorConfiguration
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
