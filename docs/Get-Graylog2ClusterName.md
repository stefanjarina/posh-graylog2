---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2ClusterName

## SYNOPSIS
Get the cluster name

## SYNTAX

```
Get-Graylog2ClusterName
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2ClusterName
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
