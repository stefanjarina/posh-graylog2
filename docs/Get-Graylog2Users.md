---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2Users

## SYNOPSIS
List all users

## SYNTAX

```
Get-Graylog2Users
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2Users
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
