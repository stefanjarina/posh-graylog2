---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2Permissions

## SYNOPSIS
Get all available user permissions

## SYNTAX

```
Get-Graylog2Permissions
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2Permissions
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
