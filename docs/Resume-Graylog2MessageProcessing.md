---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Resume-Graylog2MessageProcessing

## SYNOPSIS
Pauses message processing

## SYNTAX

```
Resume-Graylog2MessageProcessing
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Resume-Graylog2MessageProcessing
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
