---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2InputTypes

## SYNOPSIS
Get all input type

## SYNTAX

```
Get-Graylog2InputTypes
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2InputTypes
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
