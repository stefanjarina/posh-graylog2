---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2Nodes

## SYNOPSIS
List all active nodes in this cluster

## SYNTAX

```
Get-Graylog2Nodes
```

## DESCRIPTION
{{Fill in the Description}}

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2Nodes
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
