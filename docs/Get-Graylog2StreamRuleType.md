---
external help file: Graylog2-help.xml
Module Name: Graylog2
online version:
schema: 2.0.0
---

# Get-Graylog2StreamRuleType

## SYNOPSIS
Display static stream rule type

## SYNTAX

```
Get-Graylog2StreamRuleType
```

## DESCRIPTION
This is used to make easier stream management

## EXAMPLES

### EXAMPLE 1
```
Get-Graylog2StreamRuleType
```

## PARAMETERS

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
