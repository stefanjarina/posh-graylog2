# posh-graylog2

PowerShell Module for interacting with Graylog2 REST API

## WORK IN PROGRESS

This module is being activelly developed and is not yet ready

## TODO

* [ ] finish exported functions
* [ ] Verify functionality of all functions with 2.4+
* [ ] update metadata
* [ ] publish to powershell-gallery
* [ ] installation instructions
* [ ] tests
* [ ] API documentation
* [ ] example scripts using this module

## CREDITS

* A lot of code is taken from [v-team/powershell-graylog2](https://github.com/v-team/powershell-graylog2) which seems to be no longer maintained
* Rewritten to work with Graylog 2.4 version
